<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\CupRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CupRepository::class)]
#[ApiResource]
class Cup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $quantity = null;

    #[ORM\ManyToOne(inversedBy: 'cups')]
    private ?user $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }
}
