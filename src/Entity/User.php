<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Entity\Bottle;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;


#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new Post(),
        new Delete(),
        new Put(),
        new GetCollection()
    ]
)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: history::class)]
    private Collection $history;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Cup::class)]
    private Collection $cups;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Bottle::class)]
    private Collection $bottles;

    public function __construct()
    {
        $this->history = new ArrayCollection();
        $this->cups = new ArrayCollection();
        $this->bottles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, history>
     */
    public function getHistory(): Collection
    {
        return $this->history;
    }

    public function addHistory(history $history): self
    {
        if (!$this->history->contains($history)) {
            $this->history->add($history);
            $history->setUser($this);
        }

        return $this;
    }

    public function removeHistory(history $history): self
    {
        if ($this->history->removeElement($history)) {
            // set the owning side to null (unless already changed)
            if ($history->getUser() === $this) {
                $history->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Cup>
     */
    public function getCups(): Collection
    {
        return $this->cups;
    }

    public function addCup(Cup $cup): self
    {
        if (!$this->cups->contains($cup)) {
            $this->cups->add($cup);
            $cup->setUser($this);
        }

        return $this;
    }

    public function removeCup(Cup $cup): self
    {
        if ($this->cups->removeElement($cup)) {
            // set the owning side to null (unless already changed)
            if ($cup->getUser() === $this) {
                $cup->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Bottle>
     */
    public function getBottles(): Collection
    {
        return $this->bottles;
    }

    public function addBottle(Bottle $bottle): self
    {
        if (!$this->bottles->contains($bottle)) {
            $this->bottles->add($bottle);
            $bottle->setUser($this);
        }

        return $this;
    }

    public function removeBottle(Bottle $bottle): self
    {
        if ($this->bottles->removeElement($bottle)) {
            // set the owning side to null (unless already changed)
            if ($bottle->getUser() === $this) {
                $bottle->setUser(null);
            }
        }

        return $this;
    }
}
